//
//  PCDModel.swift
//  AnimateCube
//
//  Created by T_O on 2018/03/24.
//  Copyright © 2018年 BurtK. All rights reserved.
//

import Foundation

//
//  ObjModel.swift
//  ObjModelLoader
//
//  Created by Michael Rommel on 30.08.17.
//  Copyright © 2017 BurtK. All rights reserved.
//

import Foundation
import GLKit

class PCDModel: Model {
    
    // init(name: String, shape: Shape, shader: BaseEffect) {
    init(name: String, shape: UnsafeMutablePointer<float7>!, shader: BaseEffect) {
        
        var vertices: [Vertex] = []
        var indices: [GLuint] = []
        var index: GLuint = 0
        
        //assert(shape.vertices.count == shape.normals.count && shape.normals.count == shape.textureCoords.count, "wrong number of vertices, normals and texture coords")
        
        let count: UInt = 100000
        
        for i:UInt in 0..<count {
            let pt2: float7! = shape?.advanced(by: Int(i)).pointee

            var v = Vertex()
            v.x = GLfloat(pt2.x)
            v.y = GLfloat(pt2.y)
            v.z = GLfloat(pt2.z)
            v.r = GLfloat(pt2.r / 255.0)
            v.g = GLfloat(pt2.g / 255.0)
            v.b = GLfloat(pt2.b / 255.0)
            v.a = GLfloat(pt2.a)
            
            vertices.append(v)
            
            indices.append(index)
            index = index + 1
        }
        
        super.init(name: name, shader: shader, vertices: vertices, indices: indices)
            // self.loadTexture((shape.material?.diffuseTextureMapFilePath?.lastPathComponent)! as String)
    }
    
    override func updateWithDelta(_ dt: TimeInterval) {
        // self.rotationZ = self.rotationZ + Float(Double.pi * dt)
        self.rotationY = self.rotationY + Float(Double.pi * dt / 8)
    }
}
