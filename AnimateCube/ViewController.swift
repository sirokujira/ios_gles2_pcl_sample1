//
//  ViewController.swift
//  AnimateCube
//
//  Created by burt on 2016. 2. 28..
//  Copyright © 2016년 BurtK. All rights reserved.
//

import UIKit
import GLKit

// import libPointCloudLibrary;

class GLKUpdater : NSObject, GLKViewControllerDelegate {
    
    weak var glkViewController : ViewController!
    
    init(glkViewController : ViewController) {
        self.glkViewController = glkViewController
    }
    
    
    func glkViewControllerUpdate(_ controller: GLKViewController) {
        self.glkViewController.cube.updateWithDelta(self.glkViewController.timeSinceLastUpdate)
    }
}


class ViewController: GLKViewController {
    
    var glkView: GLKView!
    var glkUpdater: GLKUpdater!
    var shader : BaseEffect!
    var square : Square!
    // var cube : Cube!
    var cube : PCDModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGLcontext()
        setupGLupdater()
        setupScene()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func glkView(_ view: GLKView, drawIn rect: CGRect) {
        
        //Transfomr4: Viewport: Normalized -> Window
        //glViewport(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        //이건 GLKit이 자동으로 해준다
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GLbitfield(GL_COLOR_BUFFER_BIT))
        
        let viewMatrix : GLKMatrix4 = GLKMatrix4MakeTranslation(0, -1, -5)
        //self.square.renderWithParentMoelViewMatrix(viewMatrix)
        self.cube.renderWithParentMoelViewMatrix(viewMatrix)
    }
    
    
}

extension ViewController {

    func setupGLcontext() {
        glkView = self.view as! GLKView
        glkView.context = EAGLContext(api: .openGLES2)!
        EAGLContext.setCurrent(glkView.context)
    }
    
    func setupGLupdater() {
        self.glkUpdater = GLKUpdater(glkViewController: self)
        self.delegate = self.glkUpdater
    }
    
    func setupScene() {
        self.shader = BaseEffect(vertexShader: "SimpleVertexShader.glsl", fragmentShader: "SimpleFragmentShader.glsl")
        
        self.shader.projectionMatrix = GLKMatrix4MakePerspective(
            GLKMathDegreesToRadians(85.0),
            GLfloat(self.view.bounds.size.width / self.view.bounds.size.height),
            1,
            150)
        
        
//        self.square = Square(shader: self.shader)
//        self.square.position = GLKVector3(v: (0.5, -0.5, 0))
        
        // self.cube = Cube(shader: self.shader)
        
        let pcl = PointCloudLibraryInterface();
        // pcl.callLoad("pointcloud.pcd");
        pcl.callLoadResourceFile();
        pcl.callFiltering();
        var pt_data: UnsafeMutablePointer<float7>! = pcl.getPointCloudData()
        
        let pt1: float7 = pt_data.pointee
        // UnsafeMutablePointer<float7>! data = pcl.getPointCloudData();
        // let aaa: UnsafeMutablePointer<float7> = pt_data?.advanced(by: 1);
        // UnsafeMutablePointer<float7>! aaa = pt_data;
        // aaa.;
        print("x1: \(pt1.x)")
        print("y1: \(pt1.y)")
        print("z1: \(pt1.z)")
        print("r1: \(pt1.r)")
        print("g1: \(pt1.g)")
        print("b1: \(pt1.b)")
        print("a1: \(pt1.a)")
        
        // let pt2: float7 = pt_data.pointee
        let pt2: float7! = pt_data?.advanced(by: 1).pointee;
        print("x2: \(pt2.x)")
        print("y2: \(pt2.y)")
        print("z2: \(pt2.z)")
        print("r2: \(pt2.r)")
        print("g2: \(pt2.g)")
        print("b2: \(pt2.b)")
        print("a2: \(pt2.a)")

        self.cube = PCDModel(name: "key", shape: pt_data!, shader: self.shader)
    }
    
    
}
