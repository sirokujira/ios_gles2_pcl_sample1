using System.Runtime.InteropServices;

[StructLayout (LayoutKind.Sequential)]
public struct SwiftPointXYZRGBA
{
	public float x;

	public float y;

	public float z;

	public float r;

	public float g;

	public float b;

	public float a;
}

[StructLayout (LayoutKind.Sequential)]
public struct SwiftPointXYZ
{
	public float x;

	public float y;

	public float z;
}
